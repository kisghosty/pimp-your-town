from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import url
from django.views.static import serve

from pimpyourtown import views

urlpatterns = [
    path('', views.IndexView.as_view(), name=''),
    path('city', views.CityView.as_view(), name='city')
]

# Admin
urlpatterns += [
    path('admin/', admin.site.urls),
]

# Static files
urlpatterns += [
    url(r'^static/(?P<path>.*)$', serve, {
            'document_root': settings.STATIC_ROOT
        }),
]