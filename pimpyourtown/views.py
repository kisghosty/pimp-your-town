
from django.views.generic import View, TemplateView

from django.http import HttpResponse
from django.template.response import SimpleTemplateResponse

from pimpyourtown.models import County, City

class IndexView(TemplateView):
    template_name = "index.html"
    
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['counties'] = County.objects.all()
        return context

class CityView(View):

    def getCounty(self):
        return County.objects.get(pk=self.request.POST['countyId'])

    def getCountyView(self):
        template = 'content/county_details.html'
        context = {
            'county': self.getCounty()
        }
        return SimpleTemplateResponse(template, context)

    def addCity(self):
        city = City()
        city.name = self.request.POST['newCityName']
        city.county = County.objects.get(pk=self.request.POST['countyId'])
        city.save()
        
        return self.getCountyView()

    def updateCity(self):
        try:
            newCityName = self.request.POST['newCityName']
            city = City.objects.get(pk=self.request.POST['cityId'])
            response = HttpResponse(city.name + ' renamed to ' + newCityName)
            city.name = newCityName
            city.save()
        except City.DoesNotExist:
            response = HttpResponse('No such city')
        return response

    def removeCity(self):
        try:
            city = City.objects.get(pk=self.request.POST['cityId'])
            response = HttpResponse(city.name + ' deleted')
            city.delete()
        except City.DoesNotExist:
            response = HttpResponse('No such city')
        return response

    def post(self, request, *args, **kwargs):
        response = HttpResponse('Invalid operation')
        if request.POST['action'] == "get":
            response = self.getCountyView()
        if request.POST['action'] == "add":
            response = self.addCity()
        if request.POST['action'] == "update":
            response = self.updateCity()
        if request.POST['action'] == "remove":
            response = self.removeCity()
        
        return response