$( document ).ready(function() {
    $('#county-select').on('change', function() {
        $('#addCityForm').show()
        $('#countyDetailsContainer').show()

        $('div[id^="countyDetails_"]').hide();
        $('#countyDetails_' + $(this).val() ).show()
    });
    


    $('#addCityForm #addCityBtn').on("click", function(e){
        e.preventDefault()

        newCityName = $("#addCityForm input[name='city_name']").val()
        // TODO : Fix hardcoded url
        $.post(
            "/city", 
            {
                csrfmiddlewaretoken : $("#addCityForm input[name='csrfmiddlewaretoken']").val(),
                action : "add",
                countyId : $('#county-select').val(),
                newCityName : newCityName
            }
            ).done(function(result) {
                $("#countyDetailsContainer").append($("#editCityForm").hide())
                resultContainer = $("#countyDetails_" + $('#county-select').val())
                resultContainer.html( result )
                initCityLabels($('#county-select').val())
            });
    });

    $('#editCityForm #cancelBtn').on("click", function(e){
        e.preventDefault()
        $("#editCityForm").hide()
        $(this).parents('[id^="cityListItem_"]').children(".label").show()
    });

    $('#editCityForm #updateBtn').on("click", function(e){
        e.preventDefault()
        container = $(this).parents('li[id^="cityListItem_"]')
        id = container.attr("id").replace("cityListItem_", "")
        newName = $('#editCityForm input[name="cityName"]').val()

        $.post(
            "/city", 
            {
                csrfmiddlewaretoken : $("#editCityForm input[name='csrfmiddlewaretoken']").val(),
                action : "update",
                cityId : id,
                newCityName : newName
            });

        
        $(this).parents('[id^="cityListItem_"]').children(".label").html(newName).show()
        $("#editCityForm").hide()
    });

    $('#editCityForm #removeBtn').on("click", function(e){
        e.preventDefault()
        cityContainer = $(this).parents('li[id^="cityListItem_"]')
        cityId = cityContainer.attr("id").replace("cityListItem_", "")
        // TODO : fix hardcoded url
        $.post(
            "/city", 
            {
                csrfmiddlewaretoken : $("#editCityForm input[name='csrfmiddlewaretoken']").val(),
                action : "remove",
                cityId : cityId,
            }
            ).done(function(result) {
 
            });
        $("#countyDetailsContainer").append($("#editCityForm").hide())
        cityContainer.remove()
    });
});

(initCityLabels = function(countyId="") {
    $('div[id^="countyDetails_' + countyId +'"] #cityListContainer .label').on("click", function(){
        cityName = $(this).html()
        $('div[id^="countyDetails_"] #cityListContainer .label').show()
        
        $("#editCityForm input[name='cityName']").val(cityName)
        $(this).before($("#editCityForm").show())
        
        $(this).hide()
    });
})()
