from pimpyourtown.models import County
from django.contrib.admin import register
from django.contrib.admin import ModelAdmin

from pimpyourtown.models import County, City

@register(County)
class CountyAdmin(ModelAdmin):
    pass

@register(City)
class CityAdmin(ModelAdmin):
    list_display = ('name', 'get_county', )

    def get_county(self, obj):
        return obj.county.name
    get_county.admin_order_field  = 'county'  
    get_county.short_description = 'County Name'