from django.db import models

from django.utils.translation import gettext_lazy as _

class County(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Name'), null=False, blank=False)
    
    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name=_('County')
        verbose_name_plural = _('Counties')

class City(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Name'), null=False, blank=False)
    county = models.ForeignKey(County, on_delete=models.CASCADE, null=False)
    
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']
        verbose_name=_('City')
        verbose_name_plural = _('Cities')
        
